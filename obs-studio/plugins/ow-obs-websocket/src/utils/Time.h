
#pragma once
#include <cstdint>

namespace Utils {
	namespace Time {
		uint64_t CurrentSecsSinceEpoch();
	}
}
